# Simple Application

Mobile Development in Action Submission

Build a simple application that implements any of these features : 

1. Login Page

1. Register Page

1. Welcome Page 

- E.g :  Self Biography

General Requirements

- Using Kotlin as a language

- Using a reusable component / custom view to create the layout.
- Zero warnings and errors.

Basic Requirements

- Login and Register function build with hardcoded data and credentials.

- Put and get the data in any object you created or you just hardcoded it.
- Route to welcome page when you successfully log in.

Advance Requirements

- Login and Register function you build with local storage implementation to store and get the data.
refer to this Codelab

Additional Value

- Try to use Design Pattern such as MVVM or MVI is a plus
- No Boilerplate code

Deliverables

- Use github or gitlab to push your app repository.
Send the PDF Doc with your app screenshot and URL to your repository page.
